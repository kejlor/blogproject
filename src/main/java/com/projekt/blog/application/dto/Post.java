package com.projekt.blog.application.dto;

import javax.persistence.*;

@Entity
@Table(name = "post")
public class Post {

    private int Id;
    private int PersonId;
    private String text;

    public Post(int id, int personId, String text) {
        Id = id;
        PersonId = personId;
        this.text = text;
    }
    @javax.persistence.Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }
    @Column(name = "personId")
    public int getPersonId() {
        return PersonId;
    }

    public void setPersonId(int personId) {
        PersonId = personId;
    }

    @Column(name = "text")
    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
