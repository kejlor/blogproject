package com.projekt.blog.application.dto;


public class Person {

    private String name;
    private int Id;

    public Person(int id, String name) {
        Id = id;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }
}