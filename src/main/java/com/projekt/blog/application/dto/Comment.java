package com.projekt.blog.application.dto;

import javax.persistence.*;

@Entity
@Table(name = "comment")
public class Comment {

    private int Id;
    private String text;

    public Comment(int id, String text) {
        Id = id;
        this.text = text;
    }
    @javax.persistence.Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    @Column(name = "text")
    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
