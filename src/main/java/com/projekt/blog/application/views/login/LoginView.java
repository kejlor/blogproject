package com.projekt.blog.application.views.login;

import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.login.LoginForm;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.router.BeforeEnterEvent;
import com.vaadin.flow.router.BeforeEnterObserver;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
// import com.projekt.blog.application.views.main.MainView;
// import com.vaadin.flow.component.Text;

@Route(value = "login") // ,layout = MainView.class) // - full screen disable/enable
@PageTitle("Login")
public class LoginView extends HorizontalLayout implements BeforeEnterObserver { // Div

    LoginForm login = new LoginForm();
    public LoginView() {
        setId("login-view");
        addClassName("login-view");

        //add(new Text("Content placeholder"));

        setSizeFull();
        setJustifyContentMode(JustifyContentMode.CENTER);
        setAlignItems(Alignment.CENTER);

        login.setAction("login");

        add(login);
    }

    @Override
    public void beforeEnter(BeforeEnterEvent beforeEnterEvent) {
        // inform the user about an authentication error
        if(beforeEnterEvent.getLocation()
                .getQueryParameters()
                .getParameters()
                .containsKey("error")) {
            login.setError(true);
        }
    }
}
