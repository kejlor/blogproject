package com.projekt.blog.application.views.addPost;

import com.projekt.blog.application.views.main.MainView;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.router.RouteAlias;

@Route(value = "addpost", layout = MainView.class)
@PageTitle("Add Post")
@CssImport(value = "./styles/views/posts/posts-view.css", include = "lumo-badge")
@JsModule("@vaadin/vaadin-lumo-styles/badge.js")
@RouteAlias(value = "", layout = MainView.class)
public class AddPostView {

}
