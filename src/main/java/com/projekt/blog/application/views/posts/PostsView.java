package com.projekt.blog.application.views.posts;

import java.util.Random;

import com.projekt.blog.application.dto.Comment;
import com.projekt.blog.application.dto.Post;
import com.projekt.blog.application.repository.CommentRepository;
import com.projekt.blog.application.repository.PostRepository;
import com.vaadin.flow.component.ClickEvent;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridVariant;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.function.ValueProvider;
import com.vaadin.flow.router.AfterNavigationEvent;
import com.vaadin.flow.router.AfterNavigationObserver;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.projekt.blog.application.views.main.MainView;
import com.vaadin.flow.router.RouteAlias;

@Route(value = "posts", layout = MainView.class)
@PageTitle("Posts")
@CssImport(value = "./styles/views/posts/posts-view.css", include = "lumo-badge")
@JsModule("@vaadin/vaadin-lumo-styles/badge.js")
@RouteAlias(value = "", layout = MainView.class)
public class PostsView extends Div implements AfterNavigationObserver {

    PostRepository postRepository;
    CommentRepository commentRepository;
    Grid<Post> grid = new Grid<>();

    public PostsView() {
        TextField labelField = new TextField();
        labelField.setLabel("Wprowadz swoj post");
        setId("posts-view");
        addClassName("posts-view");
        setSizeFull();
        this.postRepository = new PostRepository();
        grid.addComponentColumn((ValueProvider<Post, Component>) post -> createCard(post));
        grid.setItems(postRepository.getAllPosts());
        grid.setHeight("100%");
        grid.addThemeVariants(GridVariant.LUMO_NO_BORDER, GridVariant.LUMO_NO_ROW_BORDERS);
        add(grid);
        add(labelField);
        Button button = new Button("Dodaj post");

        button.addClickListener(new ComponentEventListener<ClickEvent<Button>>() {
            @Override
            public void onComponentEvent(ClickEvent<Button> buttonClickEvent) {
                Post post = new Post(new Random().nextInt(),new Random().nextInt(), labelField.getValue());
                if(!labelField.getValue().equals(""))
                postRepository.addPost(post);
                System.out.println(postRepository.getAllPosts());
                grid.getDataProvider().refreshAll();
            }
        });
        add(button);
    }


    private HorizontalLayout createCard(Post post) {
        HorizontalLayout card = new HorizontalLayout();
        card.addClassName("card");
        card.setSpacing(false);
        card.getThemeList().add("spacing-s");

        VerticalLayout description = new VerticalLayout();
        description.addClassName("description");
        description.setSpacing(false);
        description.setPadding(false);

        HorizontalLayout header = new HorizontalLayout();
        header.addClassName("header");
        header.setSpacing(false);
        header.getThemeList().add("spacing-s");

        HorizontalLayout actions = new HorizontalLayout();
        actions.addClassName("actions");
        actions.setSpacing(false);
        actions.getThemeList().add("spacing-s");
        Span post1 = new Span(post.getText());
        post1.addClassName("post");
        TextField commentField = new TextField();
        commentField.setLabel("Komentarz");
        Button commentButton = new Button("Dodaj komentarz");

        commentButton.addClickListener(new ComponentEventListener<ClickEvent<Button>>() {
            @Override
            public void onComponentEvent(ClickEvent<Button> buttonClickEvent) {
                Comment comment = new Comment(new Random().nextInt(), commentField.getValue());
                if(!commentField.getValue().equals(""))
                    commentRepository.addComment(comment);
                System.out.println(commentRepository.getAllComments());
                grid.getDataProvider().refreshAll();
            }
        });

        description.add(header, post1, actions, commentField, commentButton);
        card.add(description);
        return card;
    }

    @Override
    public void afterNavigation(AfterNavigationEvent event) {
        grid.setItems(postRepository.getAllPosts());
    }
}
