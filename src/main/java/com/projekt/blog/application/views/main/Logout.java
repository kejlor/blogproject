package com.projekt.blog.application.views.main;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.router.Route;

@Route(value = "logout")
public class Logout extends MainView{
    public Logout(){
        UI.getCurrent().getPage().reload();
    }
}
