package com.projekt.blog.application.repository;

import com.projekt.blog.application.dto.Post;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

@Repository
public class PostRepository {
    private Map<Integer, Post> postRepository = new HashMap<>();

    public Collection<Post> getAllPosts(){
        return postRepository.values();
    }
    public Post getPostById(int Id){
        return postRepository.get(Id);
    }
    public void addPost(Post post){
        postRepository.put(post.getId(),post);
    }
}
