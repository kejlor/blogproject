package com.projekt.blog.application.repository;

import com.projekt.blog.application.dto.Comment;
import com.vaadin.flow.component.Component;
import org.springframework.stereotype.Repository;


import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

@Repository
public class CommentRepository {
    private Map<Integer, Comment> commentRepository = new HashMap<>();

    public Collection<Comment> getAllComments(){
        return commentRepository.values();
    }
    public Comment getCommentById(int Id){
        return commentRepository.get(Id);
    }
    public void addComment(Comment comment){
        commentRepository.put(comment.getId(),comment);
    }
}
